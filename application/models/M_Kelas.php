<?php

class M_Kelas extends CI_Model
{
    public function fetch_all()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('data_kelas');
        return $query->result_array();
    }

    public function fetch_single_data($id)
    {
        $this->db->where("id", $id);
        $query = $this->db->get('data_kelas');
        return $query->row();
    }

    public function insert_api($data)
    {
        $this->db->insert('data_kelas', $data);
        return $this->db->insert_id();
    }

    public function update_data($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("data_kelas", $data);
    }

    public function check_data($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('data_kelas');
        return $query->row() ? true : false;
    }

    public function delete_data($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("data_kelas");

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
?>
